import json, chevron

ENV = "local"
ENV_DETAILS = {
    "local": {"environmentLevel": "local", "db2SqlSchema":  "db2tst2", "db2ProcSchema":  "TST2"},
    "tenv1": {"environmentLevel": "support", "db2SqlSchema":  "db2test", "db2ProcSchema":  "TST1"},
    "tenv2": {"environmentLevel": "it", "db2SqlSchema":  "db2tst2", "db2ProcSchema":  "TST2"},
    "tenv3": {"environmentLevel": "uat", "db2SqlSchema":  "db2tst3", "db2ProcSchema":  "TST3"},
    "tenv6": {"environmentLevel": "it", "db2SqlSchema":  "db2tst6", "db2ProcSchema":  "TST6"},
    "tenv7": {"environmentLevel": "qa", "db2SqlSchema":  "db2tst7", "db2ProcSchema":  "TST7"},
    "tenvb": {"environmentLevel": "qa", "db2SqlSchema":  "db2tstb", "db2ProcSchema":  "TSTB"},
    "tenvc": {"environmentLevel": "uat", "db2SqlSchema":  "db2tstc", "db2ProcSchema":  "TSTC"},
    "penv": {"environmentLevel": "prod", "db2SqlSchema":  "db2prod", "db2ProcSchema":  "PBS", "cyberArk":  ""}
}

CONFIG_MAP = {}
TOKEN_REPLACEMENTS = {}

def readInput(file_name):
    with open(file_name, "r") as infile:
        input_file = json.load(infile)
    return input_file

def populate_global(global_vals):
    for key in global_vals.keys():
        CONFIG_MAP[key] = global_vals[key]
    return 0

def populate_tokens(token_vals):
    for key in token_vals.keys():
        TOKEN_REPLACEMENTS[key] = token_vals[key]
    return 0

def env_specific_vars(env_vals):
    env_val_dict = {}
    for key in env_vals.keys():
        env_val_dict[key] = env_vals[key]
    return env_val_dict

def update_globals(global_vals, env_vals):
    for key in env_vals:
        global_vals[key] = env_vals[key]
        
def token_replace(global_vals, token_vals):
    global_vals_string = str(global_vals)
    args = {
        'template': global_vals_string,
        'data': {            
        }
    }
    args["data"]["environment"] = ENV
    for key in ENV_DETAILS[ENV]:
        args["data"][key] = ENV_DETAILS[ENV][key]
    for key in token_vals:
        args["data"][key] = token_vals[key]
    return chevron.render(**args)

def mod_global_vals(global_vals):
    for key in global_vals:
        cmd = str(global_vals[key]).split(":")[0]
        if cmd == "upper":
            global_vals[key] = str(global_vals[key]).split(":")[1].upper()
        elif cmd == "lower":
            global_vals[key] = str(global_vals[key]).split(":")[1].lower()
        elif cmd == "replace":
            cmd_case = str(global_vals[key]).split(":")[2]
            val1 = str(global_vals[key]).split(":")[1].split(",")[0].replace("[","")
            val2 = str(global_vals[key]).split(":")[1].split(",")[1].replace("]","")
            if cmd_case == "upper":
                global_vals[key] = str(global_vals[key]).split(":")[3].replace(val1, val2).upper()
            elif cmd_case == "lower":
                global_vals[key] = str(global_vals[key]).split(":")[3].replace(val1, val2).lower()
            else:
                global_vals[key] = str(global_vals[key]).split(":")[3].replace(val1, val2).upper()
        else:
            pass


def main():
    f = readInput("./input.json")
    populate_global(f["global"])
    populate_tokens(f["varz"])
    vals = env_specific_vars(f[ENV])
    update_globals(CONFIG_MAP, vals)
    _config_map = json.loads(token_replace(CONFIG_MAP, TOKEN_REPLACEMENTS).replace("'", '"'))
    mod_global_vals(_config_map)
    with open("./configmap.json","w") as outfile:
        outfile.write(json.dumps(_config_map, indent=4))

if __name__ == "__main__":
    main()
